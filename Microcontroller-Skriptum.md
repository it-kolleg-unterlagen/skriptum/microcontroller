---
title: Skriptum
subtitle: Microcontroller
author: Stolz Stefan, Mr. Phil
rights: 
language: de-AT
keywords: [TINF, Microcontroller]
titlepage: true
titlepage-color: "36A1F0"
titlepage-text-color: "FFFFFF"
page-background: "background.pdf"
table-use-row-colors: true
toc-own-page: true
header-right: "Microcontroller"
logo: "images/Arduino-Logo.png"
---

# Diskretisierung
![Diskretisierung](images/Diskretisierung.png){height=400px width=500px}

## Digitalisierung 

Die moderne Informationstechnik unterscheidet sich von der früheren, etwa dem klassischen Telephon, durch die Digitalisierung der Information. Der Unterschied zwischen **"analog"** und **"digital"** liegt darin, dass analoge Größen jeden Wert auf einer bestimmten Skala annehmen können, während digitale Größen nur endlich viele Werte annehmen können, in der Regel 2 (digital) oder eine andere endlich Zahl, was wir im folgenden allgemein diskret (discrete) nennen. Daher ist eine diskrete Größe im Prinzip ungenauer. Dennoch hat sich dieses technologische Prinzip heute mit großem Erfolg durchgesetzt. 

Der Grund liegt im wesentlich darin, dass ein analoger Wert nur im Prinzip beliebig genau sein kann. Schon die Speicherung, aber noch mehr die Übertragung der Information führt stets zu einer Verfälschung der Information, mit dem Ergebnis, dass Fehler auftreten. Da Information durch Energie (elektrische Ladung, Licht) dargestellt und daher ständig verändert wird, muss die Information ständig regeneriert werden, in der Regel durch Verstärkung. Jede Verstärkung der gewünschten Information führt aber auch zu einer Verstärkung der Verfälschung dieser Information. Deshalb waren früher "Ferngespräche", die über viele Vermittlungsstellen geführt werden mussten, nur schwer verständlich, weil der Empfänger vielfach verstärkte Verfälschungen erhielt. 

### Beispiel 

Ein Signalwert betrage 10 V. Auf jeder Leitung werde der Signalwert um 50 % gedämpft und um 5 % verfälscht. Vor dem ersten Regenerator beträgt der Signalwert somit 5,5 V, nach Verstärkung 11 V. Nach der zweiten Verstärkung erhalten wir \underline{(11 V/2 + 0,55 V)*2 = 12,1 V}. Man erhält die folgende Tabelle: 

| 0   | 1   | 2    | 3     | 4     | 5     | 6     | 7     |
| --- | --- | ---- | ----- | ----- | ----- | ----- | ----- |
| 10  | 11  | 12,1 | 13,31 | 14,64 | 16,11 | 17,72 | 19,49 |


Man sieht, dass nach der achten Verstärkung bereits der Fehler einer 5 prozentigen Verfälschungen das Signal zu 100 % verfälscht hat. 

\pagebreak
## Diskretisierung

Im Gegensatz hierzu geht man bei der Diskretisierung davon aus, dass ein Signal nur wenige Werte besitzt. Ein Fehler bei einer Übertragung, soweit er nicht allzu gravierend ist, kann vom Empfänger leicht erkannt und entsprechend korrigiert werden. 

### Beispiel 

Digitale Signale besitzen nur zwei Werte, die meistens mit 0 und 1 bezeichnet werden. Beispielsweise könnte der 0-Wert mit einer Spannung im Bereich von 0 V bis 1 V dargestellt werden, der 1-Wert mit einer Spannung von 2 V oder größer, wobei der übliche Wert 5 V betrage. Jeder Wert zwischen 1 und 2 V ist undefiniert. Wird jetzt ein Signal von 5 V um 50 % gedämpft, so erhalten wir 2,5 V; wird es zusätzlich um 5 % verfälscht, so liegt es noch immer im Bereich von 2,25 V bis 2,75 V. Nach einer Verstärkung sollte es aber wieder einen Wert von ca. 5 V erhalten, so dass die Information, nämlich 0-Wert oder 1-Wert, unverfälscht zum Empfänger übertragen wird. Der Empfänger kann das regenerierte Signal weitersenden, wo es wieder regeneriert wird usw., ohne dass überhaupt eine Verfälschung der Information auftritt. 

Die meiste Information, z.B. Sprachinformation, besteht aus sehr vielen verschiedenen Werten. Um diese zu diskretisieren, wird zunächst ein gewisses Raster vorgegeben, auf welches die analogen Signale abgebildet werden. Z.B. ließen sich die Werte eines Sprachsignals von –10 V bis +10 V auf 201 diskrete Werte (-10 V, -9,9 V, -9,8 V, ... –0,1 V, 0.0 V, 0,1 V, ... 9,9 V, 10,0 V) abbilden. Die Rundung werde beispielsweise durch den "nahesten diskreten Wert" festgelegt. Diese Werte könnten jetzt durch digitale Werte dargestellt werden. Mit acht 1-Bit Werten lassen sich 28=256 verschiedene Werte unterscheiden. Daher lässt sich jeder analoge Wert aus diesem Beispiel auf ein Byte abbilden. Diese Bytes können jetzt bitweise digital übertragen werden, und der Empfänger kann jedes einzelne Bit stets wieder genau restaurieren, also einen völlig unverfälschten Wert erhalten. Dieses kann auch über mehrere Zwischenverstärker geschehen, so dass insgesamt die digitale Übertragung analoger Signale eine relevante Verbesserung der Qualität gegenüber der analogen Übertragung bedeutet. 

Dieses Prinzip wird, mit einigen technischen Verbesserungen, im ISDN verwendet, welches heute allgemein sowohl zur Sprach- als auch zur Datenübertragung zur Verfügung steht. 

Auch die Speicherung analoger Information wird durch eine Digitalisierung erst wirtschaftlich möglich. Wurden früher spezielle Geräte verwendet, um Sprachsignale zu speichern, beispielsweise Tonbandgeräte, so wird etwa in der Zeitansage heute ausschließlich digital gespeicherte Sprache verwendet. Selbst Anrufbeantworter speichern heute die Ansagen und die Nachrichten wirtschaftlicher digital als analog. Mittlerweile kann durch die Entwicklung der Speichertechnik und Verbesserung von Kodierungsverfahren auch Video, etwas normales Fernsehen, digital gespeichert werden.  

\pagebreak
# LED ansteuern
## LED Anode Cathode 

![LED Cathode Anode](images/LED_Cathode_Anode.png){height=300px width=400px}

### Code zum Bild
```c++
// Variablen Definition für die LED
#define blue 7

void setup()
{
  // LED aktiv schalten 
  pinMode(blue, OUTPUT);
}
void loop()
{
  // LED blue einschalten 
  digitalWrite(blue, HIGH);
  // 500ms warten
  delay(500);
  // LED blue ausschalten 
  digitalWrite(blue, LOW);
  // 1000ms warten
  delay(1000);
}
```
\pagebreak
# Protothreading
## Getimte Ausführung
Es wird anstatt von Kontextwechseln getimte Ausführung durchgeführt. 

## Protothreading
Protothreading ist nicht preemive und kommt ohne Kontexwechsel durch das Betriebssystem aus. Es werden C-Macros anstatt von Funktionen verwendet, um es zu ermöglichen die flow control zu verändern.

## Unterlagen 
### Linksammlung
* [Library für getimte Methoden](https://playground.arduino.cc/Code/TimedAction/)
* [Adam Dunkels protothreading](http://dunkels.com/adam/pt/expansion.html)

\pagebreak
# Spannungsteiler
## Unterlagen 

Die meisten Sensoren ändern in Abhängigkeit eines Umweltparameters den Widerstand. Beispiel LDR (Light Density Resistor), der den Widerstand verringert, wenn Licht auf ihn fällt. 

Die Spannungsteilerschaltung wird benötigt, weil der Arduino nur eine Spannungsänderung messen kann. Durch einen Widerstand ändert sich die Spannung eines Gesamtsystems nicht. Die Spannung teilt sich aber gleichmäßig zwischen mehreren in Serie geschalteten Widerständen auf. 

### Linksammlung

* [Parallelschaltung von Widerständen](https://www.elektronik-kompendium.de/sites/slt/0110192.htm) 

* [Reihenschaltung von Widerständen](https://www.elektronik-kompendium.de/sites/slt/0110191.htm) 

* [Spannungsteiler Berechnung](https://www.peacesoftware.de/einigewerte/spannungsteiler.html) 

* [analogRead() Funktion Arduino](https://www.arduino.cc/reference/en/language/functions/analog-io/analogread/) 

## Schaltungen 

Als Widerstand empfiehlt sich ein 1K Ohm Widerstand

![Spannungsteilerschaltung](images/spannungsteiler.png)


\pagebreak
# Bus Systeme
## SPI
[SPI](https://www.arduino.cc/en/reference/SPI) wird von Sensoren oder Aktoren verwendet. Gerade wenn diese hohe Geschwindigkeiten erfordern wie Radio Module (z.B. NRF24L01).

* Voll Duplex fähig
* Hohe Geschwindigkeit möglich

Leitungen:

* **MISO** (Master In Slave Out)
* **MOSI** (Master Out Slave In)
* **SCK** (Serial Clock)
* **SS** (Slave Select) - LOW meint, dass dieses Slave mit dem Master kommuniziert.

|   |     |
|---| --- |
| ![SPI Arduino](images/arduino-spi-esquema-cascada.png){height=200px width=300px}  | ![SPI Arduino](images/Circuit-Diagram-for-SPI-Communication-between-Two-Arduinos.png){height=200px width=350px}  | 
| SPI Schema ([Quelle](https://www.rc-modellbau-portal.de/index.php?threads/spi-schnittstelle-beim-arduino-co.6994/#post-166425)) | Zwei Arduino durch SPI verbunden ([Quelle](https://circuitdigest.com/microcontroller-projects/arduino-spi-communication-tutorial)) |

\pagebreak
## I2C
Mit Hilfe der [Wire Library](https://www.arduino.cc/en/Reference/Wire) kann der Arduino I2C benutzen.

* Nur zwei Kabel notwendig
* Langsam
* Adressierung durch Protokoll
* Datenleitung (SDA) und eine Taktleitung (SCL)

### Protokoll
Die Adresse ist das erste vom Master gesendete Byte. Dieses wird nach dem Start Signal gesendet und beinhaltet auch die Information, ob gelesen oder gesendet werden soll. Slave bestätigt das Signal. Mit Stop Signal wird Übertragung beendet. 

|   | 
|---|
| ![I2C Arduino](images/I2C_Schema.jpg){height=200px width=300px}  | 
| Mehrere Sensoren über I2C mit Arduino verknüpft ([Quelle](http://www.netzmafia.de/skripten/hardware/Arduino/Programmierung/wire_library.html)) |

\pagebreak
# Schiebereister
## Flipflop

Ein [Schieberegister](https://www.elektronik-kompendium.de/sites/dig/0210211.htm) ist auf **[Flipflops](https://de.wikipedia.org/wiki/Flipflop)** aufgebaut. Dabei handelt es sich um eine elektronische Schaltung, die zwei stabile Zustände als Ausgangssignal besitzt (ein Bit). Dadurch können Daten auf unbegrenzte Zeit gespeichert werden. Der Speicher ist jedoch flüchtig, also muss die Spannung aufrecht bleiben.

Fliplops werden zum Beispiel in Mikroprozessor, statischem RAM und auch im Schieberegister eingesetzt. 

### RS-Flipflop mit Taktpegel­steuerung

| **Schaltzeichen** | **Signal-Zeit-Diagramm** | **Funktionstabelle**  |
| :---: | :---: | :---: |
| ![SER Flipflop](images/Gated_SR_flip-flop_Symbol.svg.png){height=100px width=100px} | ![SZ Diagramm](images/SR_latch_impulse_diagram.png){height=200px width=230px} |  ![Schieberegister Logik](images/shiftreg_logic.png){height=200px width=230px} |


|   |
|---|
|  ![Schieberegister Schema](images/shiftreg_schema.png) |
| Schieberegister mit taktpegelgesteuertem RS-Flipflop aufgebaut  |

\pagebreak
## 74HC595-Schiebereister
Die Schaltung für ein 74HC595 ist in der **[Arduino Homepage](https://www.arduino.cc/en/tutorial/ShiftOut)** nachzulesen. 

 
![74HC595 Mapping](images/74HC595.png){height=500px width=600px}

\pagebreak
# Literatur und Quellenverzeichniss
## Literaur: 
* **Ersteller [original Texte]**:
  * [Gitlab_Stefan Stolz](https://gitlab.com/st.stolz)

* **Ersteller [PDF]**:
  * [Github_Mr-Phil1](https://github.com/Mr-Phil1/)
  * [Gitlab_Mr-Phil1](https://gitlab.com/Mr-Phil1/)

* **GitLab Seite**:
  * [Gitlab-Seite_Mikrocontroller](https://gitlab.com/hak-imst/stol/kolleg-education/kol-tinf/-/tree/master/Mikrocontroller)
  
* **Arduino-Webseite**:
  *  [Arduino Webseite_Main](https://www.arduino.cc/)
  *  [Arduino Webseite_Getting Started with Arduino UNO](https://www.arduino.cc/en/Guide/ArduinoUno)
  *  [SPI](https://www.arduino.cc/en/reference/SPI)
  *  [Wire Library](https://www.arduino.cc/en/Reference/Wire)

* **Online Simulations Seite**:
  * [Logicly-Online Demo](https://logic.ly/demo)
  * [Similar-Download Seite](https://www.similar.at/)

## Quellen:
* **Quellen der Theorien**
  * [Diskretisierung_Informatik Uni-Oldenburg](http://einstein.informatik.uni-oldenburg.de/rechnernetze/digitali.htm)
  * [RS-Flipflop_Wikipedia-Deutsch](https://de.wikipedia.org/wiki/Flipflop#RS-Flipflop)
* **Grafiken**
  * [Arduino Logo_Wikimedia](https://commons.wikimedia.org/wiki/File:Arduino_Logo.svg)
  * [SPI Schema](https://www.rc-modellbau-portal.de/index.php?threads/spi-schnittstelle-beim-arduino-co.6994/#post-166425)
  * [Zwei Arduino durch SPI verbunden](https://circuitdigest.com/microcontroller-projects/arduino-spi-communication-tutorial)
  * [Mehrere Sensoren über I2C mit Arduino verknüpft](http://www.netzmafia.de/skripten/hardware/Arduino/Programmierung/wire_library.html)
  * [RS-Flipflop (Schaltzeichen+Signal-Zeit-Diagramm+Funktionstabelle)_Wikipedia-Deutsch](https://de.wikipedia.org/wiki/Flipflop#RS-Flipflop)