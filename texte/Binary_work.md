```c 

// Maske erstellen mit einer 1 auf der Stelle (k), die man holen will 

// dafür shiftet man eine 1 um k nach links 

int mask =  1 << k; 
 

// Den Wert n maskieren, indem logisches und zwischen Zahl und Maske angewandt wird 

int masked_n = n & mask; 
 

// Das Bit holen, indem der maskierte Wert wieder um k nach rechts geshiftet wird 

int thebit = masked_n >> k; 

``` 