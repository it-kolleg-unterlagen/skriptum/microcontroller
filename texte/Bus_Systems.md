# Bus Systeme

## SPI

[SPI](https://www.arduino.cc/en/reference/SPI) wird von Sensoren oder Aktoren verwendet. Gerade wenn diese hohe Geschwindigkeiten erfordern wie Radio Module (z.B. NRF24L01).

* Voll Duplex fähig
* Hohe Geschwindigkeit möglich

Leitungen:

* MISO (Master In Slave Out)
* MOSI (Master Out Slave In)
* SCK (Serial Clock)
* SS (Slave Select) - LOW meint, dass dieses Slave mit dem Master kommuniziert.

|   | 
|---|
| ![SPI Arduino](images/arduino-spi-esquema-cascada.png)  | 
| SPI Schema ([Quelle](https://www.rc-modellbau-portal.de/index.php?threads/spi-schnittstelle-beim-arduino-co.6994/post-166425) |

|   | 
|---|
| ![SPI Arduino](images/Circuit-Diagram-for-SPI-Communication-between-Two-Arduinos.png)  | 
| Zwei Arduino durch SPI verbunden ([Quelle](https://circuitdigest.com/microcontroller-projects/arduino-spi-communication-tutorial)) |

## I2C

Mit Hilfe der [Wire Library](https://www.arduino.cc/en/Reference/Wire) kann der Arduino I2C benutzen.

* Nur zwei Kabel notwendig
* Langsam
* Adressierung durch Protokoll
* Datenleitung (SDA) und eine Taktleitung (SCL)

### Protokoll

Zuerst erfolgt ein Startsignal. Danach wird die Adresse (7 Bit) im ersten vom Master gesendeten Byte übertragen. Dieses beinhaltet auch die Information, ob gelesen oder gesendet werden soll (1 Bit). Slave bestätigt das Signal. Mit Stop Signal wird Übertragung beendet. 

|   | 
|---|
| ![I2C Arduino](images/I2C_Schema.jpg)  | 
| Mehrere Sensoren über I2C mit Arduino verknüpft ([Quelle](http://www.netzmafia.de/skripten/hardware/Arduino/Programmierung/wire_library.html)) |