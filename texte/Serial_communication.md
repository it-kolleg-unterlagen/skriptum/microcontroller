## Serielle Kommunikation

Bei einer seriellen Übertragung von Daten werden die Bits nacheinander über eine Leitung geschickt, wobei 1 aktivierter Spannung entspricht (Arduino 5V) und 0 einer Spannung von 0V. 

Die **Baudrate** gibt dabei an wie oft der Wert abgetastet wird. Eine Baudrate von 9600 enrspricht einer Abtastrate von 9600 Werten in der Sekunde. Es ist notwendig diesen Wert mit der Gegenstelle zu vereinbaren. Sonst wäre es nicht möglich etwa mehrere 0 Bits hintereinander zu schicken, da nicht klar wäre, nach welcher Zeit ein neuer Bit beginnt. 

|   | 
|---|
| ![Baudrate](images/Bitrateequalbaudrate.png)  | 
| (Quelle: https://bytesofgigabytes.com/embedded/bit-rate-and-baud-rate/) |
